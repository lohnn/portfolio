<?php

function create_page($name, $imageSrc, $text)
{
    $toReturn = array("name" => $name, "imageSrc" => $imageSrc, "text" => $text);
    return $toReturn;
}

$arr = array(
    create_page("Portfolio", "./images/portfolio.jpg", file_get_contents("pages/portfoliot.md")),
    create_page("Lohnn.se", "./images/lohnn.png", file_get_contents("pages/lohnn.md")),
    create_page("Husmys.se", "./images/husmys.png", file_get_contents("pages/husmys.md")),
    create_page("Spel från spelkurs", "./images/game-course.png", file_get_contents("pages/spelkurs.md")),
    create_page("Projektarbete spel", "./images/project_game.png", file_get_contents("pages/projektarbete_spel.md")),
    create_page("Shader-programmering", "./images/Shader_guitar.png", file_get_contents("pages/shader.md")),
    create_page("Projekt i Blender", "./images/blender.jpg", file_get_contents("pages/blender.md")),
    create_page("RIA-utveckling med JavaScript", "./images/ria-js.png", file_get_contents("pages/ria_js.md")),
);
echo $_GET['callback'] . "(" . json_encode($arr) . ");";