På distans läste jag kursen 3D-modellering och animation i Open Source-miljö, där vi gick igenom open source-programmet Blenders all funktionalitet.
Blender är ett program jag använt mig mycket av på fritiden, även innan den här kursen.
Trots detta var det flera funktioner jag lärde känna som jag inte tidigare kände till.

Första halvan av kursen bestod av laborationer där vi fick lära känna miljön och dess olika funktionalitet.
Andra halvan var ett projekt där vi skulle skapa en animation.
Jag valde att göra en animation som skulle föreställa en intro-video för ett påhittat företag som gjort ett spel.
Min inspiration var sådana som Bullfrog Productions.


Slutgiltiga renderingen går att hitta på:
https://www.youtube.com/watch?v=0bOsNrp2PvI

Projektfilerna kan man hitta [här](https://drive.google.com/file/d/0B5qqXCfGf_zeRFRqR2phSG8wcFU/view?usp=sharing).