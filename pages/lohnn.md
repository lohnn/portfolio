Det här är ett av mina första programmeringsprojekt. Jag utvecklade en hemsida åt min far, Jonas Löhnn, som behövde
en portal som politiker. Medan många andra politiker har bloggar ville han ha något liknande, men väldigt skräddarsytt.
Jag hade mer eller mindre ingen erfarenhet av PHP eller JavaScript vid det här laget och bestämde mig för att anta
utmaningen att skapa webbplattformen med just dessa språk för att lära mig.

Efter några månaders intensivt utvecklande var webbsidan redo för lansering. Den innehåller funktionalitet
som automatisk generering av RSS-flöde, möjlighet att att skriva kommentarer och få svar på dessa
(dessa kommentarer måste dessutom godkännas av ägaren innan de publiceras, för att slippa spam och kommentarer som inte
hör hemma på publika forum) samt "senast skrivet" som samlar ihop alla ändringar som gjorts på sidan (uppdateringar och nya sidor).

Bakomliggande finns det dessutom många funktioner som inte syns för någon annan än ägaren av sidan. Funktioner som gör
det enkelt att skapa, ta bort och flytta på sidorna som syns i menyn. Sidredigeraren programmerade jag som en enkel
WYSIWYG-redigerare.

Även om layouten blev lyckad och funktionerna var smarta, så anser jag idag att koden jag skrev då kan göras mycket bättre, då jag aldrig jobbat med
PHP eller JavaScript innan.

http://lohnn.se/