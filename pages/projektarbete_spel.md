Som examensarbete i mina universitetsstudier jobbade jag i grupp med två klasskamrater och gjorde ett spel.

Spelets genre är något som vanligtvis kallas dungeon-crawler, och vår inspiration var spel som Diablo, Torchlight och Path of Exile.
Vi använde oss av spelmotorn Unity3D.
Då jag redan hade lite erfarenhet av Unity3D så kom jag att fungera som informell projektledare, och mina arbetskamrater kom till mig när de undrade något.

Vår uppdelning var att jag ansvarade övergripande över projektet, med största fokus på programmeringen.
Mina arbetskamrater gjorde sina huvudsakliga insatser inom programmering respektive grafik.

De grafiska delarna skapade vi med programmen Gimp och Blender, då det är program jag känner mig väldigt hemma och därmed kunde stödja övriga med.


https://bitbucket.org/lohnn/game-project/