Under andra året på universitetet hade vi en kurs i spelprogrammering.
När mina klasskamrater satt och knåpade med spel i 2D satte jag mig med en 3D-motor och gjorde mina uppgifter i tre dimensioner.

Kursen slutade i ett projekt, ett spel som jag uppfann med touch (mobiler och surfplattor) i åtanke.
Jag byggde upp ett pusselspel där man styr en boll genom att placera ut pilar som pekar åt vilket håll bollen ska färdas.
Dessa ska placeras ut innan bollen sätts i rullning, och sedan ska man lyckas föra bollen till mål.



https://bitbucket.org/lohnn/spelprogrammering-projekt