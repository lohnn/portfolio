Med den här webbsidan visar jag exempel på några mindre projekt som jag genomfört under mina olika steg i utvecklingen
till programmerare. Sidan visar, utan att lägga någon större vikt vid layout, hur jag bygger funktionalitet i JavaScript,
med hjälp av bland annat React, React-Router, Less och AJAX.

Datan hämtar sidan från ett litet PHP-script som körs på en Apache-server, vars enda funktion är att skicka sidinformationen
tillbaks till frontend.

Exemplen ligger i kronologisk ordning.

https://bitbucket.org/lohnn/portfolio/