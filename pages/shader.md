Under kursen shaderprogrammering lärde vi oss programmera OpenGL-shaders.
Det var valfritt vilket OpenGL-API vi ville använda oss av, kursens genomgångar var i WebGL.
Alla valde att använda sig av WebGL, utom jag.
Jag bestämde mig för att göra mitt JOGL, delvis för att jag ville utmana mig själv och delvis för att jag ville testa teknologin bakom OpenGL 4.
Något jag inte hade kunnat göra i WebGL då det är bundet till OpenGL ES 2.0.

I kursen ingick även att flytta delar av våra objekt i en animation.
Därför tar jag isär en gitarr jag skapat i programmet Blender.
För att kunna ladda in den i mitt Java-program exporterade jag mitt 3D-objekt till .obj-filer.
Då jag inte hittade någon bra .obj-importerare i Java som fungerade tillsammans med mitt projekt, behövde jag skriva en sådan för ändamålet.
Vi skulle även lära oss olika renderingsmetoder, såsom anaglyfisk och stereorendering.
Bilden ovan visar gitarren i dess olika lägen, renderad i olika renderingsmetoder.

Programmet är skrivet för att vara dynamiskt och du styr allt i programmet, såsom renderingsmetod, animering av gitarr samt positionering av kamera och rotation av objekt vid körning.


All kod för projektet finns att hitta på:
https://bitbucket.org/lohnn/shader-course/