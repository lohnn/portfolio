Under utbildningen valde jag att gå kursen som heter RIA-utveckling med JavaScript.

Kursen var uppbyggd som ett större projekt där man ägnade sig åt att bygga på sin webb-app under hela kursens gång.
Jag valde att programmera en app som agerar kassaapparat.
Mitt mål var att göra den så intuitiv som möjlig samt så enkelt som möjligt kunna ändra i produktkatalogen.

Innan kursen började var jag inte vän av JavaScript, något som faktiskt ändrades under kursens gång.
Som krav på kursen skulle vi använda oss av ett par förbestämda bibliotek, däribland [React](https://facebook.github.io/react/), [Firebase](https://www.firebase.com/) samt [Gulp](http://gulpjs.com/).
Dessa bibliotek har fått mig att mer uppskatta JavaScript igen.
React använder jag mig av i nästan alla webbsidesprojekt nu, medan jag använder Gulp i somliga och Firebase i endast ett fåtal.


Källkod finns att hitta på projektets GitHub-sida:
https://github.com/lohnn/RIA-JS