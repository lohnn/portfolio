HusMys gruppen är en samling företag som önskade en samlad webbprofil utåt.
De ville ha varsin uppdelad del av sidan där de kunde förklara sitt eget företag samt visa bilder på projekt de gjort.
I ett nära sammarbete med parterna valde vi att dela upp projekten i olika "mappar",
så de kan ladda upp sina projektbilder till dessa och få dem samlade tillsammans och sedan visas upp som ett bildspel.

Då gruppen består av fler företag behövdes inloggningsmöjlighet, samt administrator-rättigheter.
Administratörer kan lägga till och ta bort företag samt, vid behov, ändra i alla företags sidor.

Backend är det här projektet byggt i PHP.


http://husmys.se/