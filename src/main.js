var App = require('./components/app');
var React = require('react');
var ReactRouter = require("react-router"); // { Router, Route } from 'react-router';
var Route = ReactRouter.Route;
var Router = ReactRouter.Router;

/*
      <Route path="about" component={About} />
      <Route path="inbox" component={Inbox} />
*/

React.render((
  <Router>
    <Route path="/" component={App}>
    	<Route path="pages/:page" component={App} />
    </Route>
  </Router>
), document.body);