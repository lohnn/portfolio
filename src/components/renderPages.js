var React = require('react');
//var Page = require("./page");
var _ = require("lodash");
var marked = require("marked");
var $ = require("jquery");
var ReactRouter = require("react-router"); // { Router, Route } from 'react-router';
var Link = ReactRouter.Link;

//==============================================================================

var RenderPages = React.createClass({
    isRunningAnimation: false,
    currentlySelected: null,

    componentWillMount: function () {
        $(document).keyup(function (e) {
            if (e.which === 27){
                this.unselectPages();
                location.hash = "/";
            }
        }.bind(this));
    },

    componentDidMount: function() {
        this.currentlySelected = this.props.currentPage;
    },

    unselectPages: function () {
        this.currentlySelected = undefined;
        this.forceUpdate();
    },

    addPage: function (page, pid) {
        var className = "page_not_selected";
        if(this.currentlySelected !== undefined)
            className = (pid == this.currentlySelected)? "page_selected" : "page_animate_hide";
        var rawMarkup = marked(page.text, {sanitize: true});
        return <div className={className + " page"} to={"/pages/" + pid} key={pid} onClick={function(){
                    location.hash = "/pages/" + pid;
                    this.currentlySelected = pid; this.forceUpdate();
                }.bind(this)}>
                    <img className="page_image" src={page.imageSrc}/>

                    <h2 className="page_name">{page.name}</h2>
                    <span className="page_text" dangerouslySetInnerHTML={{__html: rawMarkup}}/>
                </div>;
    },

    render: function () {
        var temp;
        if(this.currentlySelected !== undefined)
            temp = <Link className="page_back_button" to="/"
                onClick={function(event){this.unselectPages(); event.stopPropagation();}.bind(this)}>
                    X
            </Link>;

        return <div>
            <div className="pages">
                {_.map(this.props.pages, this.addPage)}
            </div>
            {temp}
        </div>;
    }
});

module.exports = RenderPages;
