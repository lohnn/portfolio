var React = require('react');
var RenderPages = require('./renderPages');
var $ = require("jquery");

//==============================================================================

var App = React.createClass({
    getInitialState: function () {
        return {
            pages: {}
        };
    },
    componentWillMount: function () {
        $.ajax({
            url: "http://lohnn.se/Johannes/portfolio/response.php",
            dataType: "jsonp",
            context: this
        }).done(function (json) {
            this.setState({pages: json});
        }).fail(function () {
            console.log("Error loading");
        });
    },

    componentWillUnmount: function () {
    },

    render: function () {
        return <div>
            <RenderPages pages={this.state.pages} currentPage={this.props.params.page}/>
        </div>;
    }
});

module.exports = App;
